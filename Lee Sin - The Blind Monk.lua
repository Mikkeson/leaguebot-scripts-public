SCRIPT_LEE_SIN_VERSION = "1.00"
--[[
 _                ____  _                 _____ _            ____  _ _           _   __  __             _    
| |    ___  ___  / ___|(_)_ __           |_   _| |__   ___  | __ )| (_)_ __   __| | |  \/  | ___  _ __ | | __
| |   / _ \/ _ \ \___ \| | '_ \   _____    | | | '_ \ / _ \ |  _ \| | | '_ \ / _` | | |\/| |/ _ \| '_ \| |/ /
| |__|  __/  __/  ___) | | | | | |_____|   | | | | | |  __/ | |_) | | | | | | (_| | | |  | | (_) | | | |   < 
|_____\___|\___| |____/|_|_| |_|           |_| |_| |_|\___| |____/|_|_|_| |_|\__,_| |_|  |_|\___/|_| |_|_|\_\

]]

-- Dependencies -------------------------------------------------------------
require "utils"
require "keys"

-- Variables ----------------------------------------------------------------
local Priority = {
	["Ashe"]= 1, ["Caitlyn"] = 1, ["Corki"] = 1, ["Draven"] = 1, ["Ezreal"] = 1, ["Graves"] = 1, ["Jayce"] = 1, ["KogMaw"] = 1, ["MissFortune"] = 1, ["Quinn"] = 1, ["Sivir"] = 1,
	["Tristana"] = 1, ["Twitch"] = 1, ["Varus"] = 1, ["Vayne"] = 1, ["Lucian"] = 1,
	
	["Ahri"] = 2, ["Annie"] = 2, ["Akali"] = 2, ["Anivia"] = 2, ["Brand"] = 2, ["Cassiopeia"] = 2, ["Diana"] = 2, ["Evelynn"] = 2, ["FiddleSticks"] = 2, ["Fizz"] = 2, ["Gragas"] = 2,
	["Heimerdinger"] = 2, ["Karthus"] = 2, ["Kassadin"] = 2, ["Katarina"] = 2, ["Kayle"] = 2, ["Kennen"] = 2, ["Leblanc"] = 2, ["Lissandra"] = 2, ["Lux"] = 2, ["Malzahar"] = 2, ["Zed"] = 2,
	["Mordekaiser"] = 2, ["Morgana"] = 2, ["Nidalee"] = 2, ["Orianna"] = 2, ["Rumble"] = 2, ["Ryze"] = 2, ["Sion"] = 2, ["Swain"] = 2, ["Syndra"] = 2, ["Teemo"] = 2, ["TwistedFate"] = 2,
	["Veigar"] = 2, ["Viktor"] = 2, ["Vladimir"] = 2, ["Xerath"] = 2, ["Ziggs"] = 2, ["Zyra"] = 2, ["MasterYi"] = 2, ["Shaco"] = 2, ["Jayce"] = 2, ["Pantheon"] = 2, ["Urgot"] = 2, ["Talon"] = 2,
	
	["Alistar"] = 3, ["Blitzcrank"] = 3, ["Janna"] = 3, ["Karma"] = 3, ["Leona"] = 3, ["Lulu"] = 3, ["Nami"] = 3, ["Nunu"] = 3, ["Sona"] = 3, ["Soraka"] = 3, ["Taric"] = 3, ["Thresh"] = 3, ["Zilean"] = 3,
	
	["Darius"] = 4, ["Elise"] = 4, ["Fiora"] = 4, ["Gangplank"] = 4, ["Garen"] = 4, ["Irelia"] = 4, ["JarvanIV"] = 4, ["Jax"] = 4, ["Khazix"] = 4, ["LeeSin"] = 4, ["Nautilus"] = 4,
	["Olaf"] = 4, ["Poppy"] = 4, ["Renekton"] = 4, ["Rengar"] = 4, ["Riven"] = 4, ["Shyvana"] = 4, ["Trundle"] = 4, ["Tryndamere"] = 4, ["Udyr"] = 4, ["Vi"] = 4, ["MonkeyKing"] = 4,
	["Aatrox"] = 4, ["Nocturne"] = 4, ["XinZhao"] = 4,

	["Amumu"] = 5, ["Chogath"] = 5, ["DrMundo"] = 5, ["Galio"] = 5, ["Hecarim"] = 5, ["Malphite"] = 5, ["Maokai"] = 5, ["Nasus"] = 5, ["Rammus"] = 5, ["Sejuani"] = 5, ["Shen"] = 5,
	["Singed"] = 5, ["Skarner"] = 5, ["Volibear"] = 5, ["Warwick"] = 5, ["Yorick"] = 5, ["Zac"] = 5
}

local Target	= nil

local Q, W, E, R = "Q", "W", "E", "R"
local qRange, wRange, eRange, rRange = nil, 700, nil, 375
local qReady, wReady, eReady, rReady = false, false, false, false

local Wards		= { 2044, 2049, 2045, 2043 }
local Warded	= 0

local Spells	= {
	Q1	= "BlindMonkQOne",
	W1	= "BlindMonkWOne",
	E1	= "BlindMonkEOne",
	
	Q2	= "blindmonkqtwo",
	W2	= "blindmonkwtwo",
	E2	= "blindmonketwo",
	
	R	= "BlindMonkRKick",
}

-- Code ---------------------------------------------------------------------
function Monk()
	if IsKeyDown(Keys.T) == 1 then
		Initiate()
	end
	
	if myHero["SpellTimeQ"] > 1.0 and myHero["SpellLevelQ"] > 0 then qReady = true else qReady = false end
	if myHero["SpellTimeW"] > 1.0 and myHero["SpellLevelW"] > 0 then wReady = true else wReady = false end
	if myHero["SpellTimeE"] > 1.0 and myHero["SpellLevelE"] > 0 then eReady = true else eReady = false end
	if myHero["SpellTimeR"] > 1.0 and myHero["SpellLevelR"] > 0 then rReady = true else rReady = false end
	
	if myHero["SpellNameQ"] == Spells.Q1 then
		qRange = 975
	elseif myHero["SpellNameQ"] == Spells.Q2 then
		qRange = 1100
	end
	
	if myHero["SpellNameE"] == Spells.E1 then
		eRange = 450
	elseif myHero["SpellNameE"] == Spells.E2 then
		eRange = 600
	end
	
	for i = 1, objManager:GetMaxNewObjects(), 1 do
		local Object = objManager:GetNewObject(i)
		
		if Object ~= nil and GetTickCount() < Warded and (string.find(Object.name, "Ward") or string.find(Object.name, "Sightstone")) then
			Cast(W, Object)
		end
	end
	
	Target = GetWeakEnemy("PHYS", qRange)
end

function OnCreateObj(object)
end

function OnDraw()
end

function Initiate()
	MoveToMouse()

	-- if rReady == false then return false end
	
	local Enemies = { }
	local InitiateTarget = nil
	
	for i = 1, objManager:GetMaxHeroes(), 1 do
		Hero = objManager:GetHero(i)
	
		if Hero ~= nil and Hero.team ~= myHero.team then
			Enemies[#Enemies + 1] = Hero
		end
	end
	
	table.sort(Enemies, InitiateSort)
	
	if Enemies[1] ~= nil and GetDistance(Enemies[1]) < qRange then
		InitiateTarget = Enemies[1]
	else
		InitiateTarget = GetWeakEnemy("PHYS", qRange)
	end
	
	if InitiateTarget == nil then return false end
	
	local Position = WardPosition(InitiateTarget)
	if Position == nil then return false end
		
	if GetDistance(Position) < 200 then 
		Cast(R, InitiateTarget)
	elseif myHero["SpellNameW"] == Spells.W1 and wReady and GetDistance(Position) < 600 then
		for i, Ward in pairs(Wards) do
			if GetInventorySlot(Ward) ~= nil and GetTickCount() > Warded then
				UseItemLocation(Ward, Position.x, 0, Position.z)
				Warded = GetTickCount() + 3000
			end
		end
	else
	end
end

function InitiateSort(x, y)
	if Priority[x.charName] == Priority[y.charName] then return x.health < y.health end
	
	return Priority[x.charName] < Priority[y.charName]
end

function WardPosition(target)
	local Towers	= { }
	local Allies	= { }
	
	for i = 1, objManager:GetMaxHeroes(), 1 do
		Hero = objManager:GetHero(i)
	
		if Hero ~= nil and Hero.team == myHero.team and Hero.charName ~= myHero.charName then
			Allies[#Allies + 1] = Hero
		end
	end
	
	for i = 1, objManager:GetMaxObjects(), 1 do
		Object = objManager:GetObject(i)
	
		if myHero.team == TEAM_BLUE then
			if Object ~= nil and string.find(Object.charName, "Order_Turret_Crystal.troy") then
				table.insert(Towers, Object)
			end
		elseif myHero.team == TEAM_RED then
			if Object ~= nil and string.find(Object.charName, "Chaos_Turret_Crystal.troy") then
				table.insert(Towers, Object)
			end
		end
	end
	
	for k, v in pairs(Towers) do Allies[k] = v end

	table.sort(Allies, function(x,y) return GetDistance(x, target) < GetDistance(y, target) end)
	
	if #Allies > 0 then
		local x,y,z = (Vector(target) - Vector(Allies[1])):normalized():unpack()
		local PosX = target.x + (x * 300)
		local PosY = target.y + (y * 300)
		local PosZ = target.z + (z * 300)
		local Position = Vector(PosX, PosY, PosZ)
		
		return Position
	end
	
	return nil
end

function Cast(spell, target)
	if target == nil then target = myHero end
	CastSpellTarget(spell, target)
end

SetTimerCallback("Monk")